A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

# Author: Qi Han 	Email: qhan@uoregon.edu

* Description:
  1. Fork repo on bitbucket, modify README.md app.py and create a new credential file
  2. A message will be printed if "name.html"file exists
  3. If get "404.html", display "File not found!"
  4. If get "403.html". display "File is fobidden!"
*
