from flask import Flask, render__template, abort

app = Flask(__name__)

@app.route("/<name>")

def hello(name):

    if "~" in name or ".." in name:
        abort (403)
    elif "html" in name:
        abort (404)
    else:
        return render_template(name)
        return "UOCIS docker demo!"

@app.errorhandler(403)
def foridden_error(error):
    return render_template('403.html')


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html')


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
